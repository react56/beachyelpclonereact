import axios from 'axios';

export default axios.create({
    //baseURL: "http://localhost:3001/api/v1/beaches"
    baseURL: "https://guarded-citadel-99716.herokuapp.com/api/v1/beaches"
})