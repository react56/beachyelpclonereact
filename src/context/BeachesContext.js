import React, {useState, createContext} from 'react';

export const BeachesContext = createContext();

export const BeachesContextProvider = (props) => {
    const [beaches, setBeaches] = useState([]);
    const [selectedBeach, setSelectedBeach] = useState([null]);

    const addBeaches = (beach) => {
        setBeaches([...beaches, beach]);
    };

    return (
        <BeachesContext.Provider 
        value={{
            beaches, setBeaches, addBeaches, selectedBeach, setSelectedBeach}}>
        {props.children}
        </BeachesContext.Provider>
    );
};