import React from 'react';
import Carousel from 'react-bootstrap/Carousel'
import "./styles.css";


const CarouselHP = () => {
    return (
<Carousel>
  <Carousel.Item>
    <img
      className="photo"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Quemado.jpg/1200px-Quemado.jpg"
      alt="First slide"
    />
    <Carousel.Caption>
      <h3>Beaches</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="photo"
      src="https://www.ahstatic.com/photos/9676_bab001_00_p_1024x768.jpg"
      alt="Third slide"
    />

    <Carousel.Caption>
      <h3>Restaurants</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="photo"
      src="https://pix10.agoda.net/hotelImages/498/498878/498878_13092507300016220281.jpg?s=1200x800"
      alt="Third slide"
    />

    <Carousel.Caption>
      <h3>Hotels</h3>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
    )
}

export default CarouselHP
