import React, { useState, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'
//import { BeachesContext } from '../context/BeachesContext';
import BeachFinder from './../../apis/BeachFinder'

const UpdateBeach = (props) => {
    const { id } = useParams();
    let history = useHistory();
    //const {beaches} = useContext(BeachesContext);
    const [name, setName] = useState("");
    const [location, setLocation] = useState("");
    const [qualityRange, setQualityRange] = useState("");


    useEffect(() => {
        const fetchData = async() => {
            const response = await BeachFinder.get(`/${id}`);
            setName(response.data.data.beaches.name)
            setLocation(response.data.data.beaches.location)
            setQualityRange(response.data.data.beaches.quality_range)
        };
        fetchData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const updatedBeach = await BeachFinder.put(`/${id}`, {
            name,
            location,
            quality_range: qualityRange,
        });
        history.push("/admin");
    };

    return (
        <div>
            <form action="">
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input value={name} onChange={e => setName(e.target.value)} id="name" className="form-control" type="text"/>
                </div>
                <div className="form-group">
                    <label htmlFor="name">Location</label>
                    <input value={location} onChange={e => setLocation(e.target.value)} id="location" className="form-control" type="text"/>
                </div>
                <div className="form-group">
                    <label htmlFor="name">Quality Range</label>
                    <input value={qualityRange} onChange={e => setQualityRange(e.target.value)} id="quality_range" className="form-control" type="number"/>
                </div>
                <button type="submit" onClick={handleSubmit} className="btn btn-primary">Add</button>
            </form>
        </div>
    )
}

export default UpdateBeach