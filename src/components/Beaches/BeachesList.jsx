import React, {useEffect, useContext} from 'react'
import BeachFinder from './../../apis/BeachFinder'
import { BeachesContext } from './../../context/BeachesContext'
import { useHistory } from 'react-router-dom';
import StarRating from '../ReviewsAndStars/StarRating';


const BeachesList = (props) => {

    const {beaches, setBeaches} = useContext(BeachesContext);
    let history = useHistory();
    
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await BeachFinder.get("/")
                setBeaches(response.data.data.beaches)
            } catch (err) {}
        }
        fetchData();
    }, []);

    const handleDelete = async (id) => {
        try {
            await BeachFinder.delete(`/${id}`);
            setBeaches(beaches.filter(beach => {
                return beach.id !== id;
            }));
        } catch (err) {
           console.log(err); 
        }
    }

    const handleUpdate = async (id) => {
        try {
            history.push(`/admin/beaches/${id}/update`)
        } catch (err) {
           console.log(err);
        }
    };

    const handleListReviews = async (id) => {
        try {
            history.push(`/admin/beaches/${id}`)
        } catch (err) {
           console.log(err);
        }
    };

    const renderRating = (beach) => {

        if (!beach.count) {
            return (
                <span className="text-warning ml-1">No reviews yet</span>  
            )
        }

        return (
        <>
        <StarRating rating={beach.average_rating} />
    <span className="text-warning ml-1">({beach.count})</span>
        </>
        )
    };

    const renderWebSiteRating = (beach) => {
        return (
        <>
        <StarRating rating={beach.quality_range} />
        </>
        )
    };

    return (
        <div className="list-group">
            <table className="table table-hover table-dark">
                <thead>
                    <tr className="bg-primary">
                        <th scope="col">Beach</th>
                        <th scope="col">Location</th>
                        <th scope="col">Website Rating</th>
                        <th scope="col">Users Rating</th>
                        <th scope="col">Edit Beach</th>
                        <th scope="col">Edit Reviews</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    { beaches && beaches.map(beach => {
                        return (
                            <tr key={beach.id}>
                            <td>{beach.name}</td>
                            <td>{beach.location}</td>
                            <td>{renderWebSiteRating(beach)}</td>
                            <td>{renderRating(beach)}</td>
                            <td><button onClick={() => handleUpdate(beach.id)} className="btn btn-warning">Update</button></td>
                            <td><button onClick={() => handleListReviews(beach.id)} className="btn btn-warning">Edit Reviews</button></td>
                            <td><button onClick={() => handleDelete(beach.id)} className="btn btn-danger">Delete</button></td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default BeachesList
