import React, { useState, useContext } from 'react'
import BeachFinder from './../../apis/BeachFinder'
import { BeachesContext } from './../../context/BeachesContext'


const AddBeach = () => {
    const {addBeaches} = useContext(BeachesContext);
    const [name, setName] = useState("");
    const [location, setLocation] = useState("");
    const [qualityRange, setQualityRange] = useState("Quality Range");

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await BeachFinder.post("/", {
                name,
                location,
                quality_range: qualityRange
            });
            addBeaches(response.data.data.beaches);
            console.log(response);
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className="mb-4">
            <form action="">
                <div className="form-row">
                    <div className="col">
                        <input value={name} onChange={e => setName(e.target.value)} type="text" className="form-control" placeholder="name"/>
                    </div>
                    <div className="col">
                        <input value={location} onChange={e => setLocation(e.target.value)} type="text" className="form-control" placeholder="location"/>
                    </div>
                    <div className="col">
                        <select value={qualityRange} onChange={e => setQualityRange(e.target.value)} className="custom-select my-1 mr-sm-2">
                            <option disabled>Quality Range</option>
                            <option value="1">#</option>
                            <option value="2">##</option>
                            <option value="3">###</option>
                            <option value="4">####</option>
                            <option value="5">#####</option>
                        </select>
                    </div>
                    <button onClick={handleSubmit} type="submit" className="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    )
}

export default AddBeach
