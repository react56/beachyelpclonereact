import React from 'react'
import AddBeach from '../../components/Beaches/AddBeach'
import BeachesList from '../../components/Beaches/BeachesList'

const AdminHome = () => {
    return (
        <div>
            <AddBeach/>
            <BeachesList/>
        </div>
    )
}

export default AdminHome
