import React from 'react'
import UpdateBeach from '../../components/Beaches/UpdateBeach'

const UpdateBeachPage = () => {
    return (
        <div>
            <h1 className="text-center">Update Beach</h1>
            <UpdateBeach />
        </div>
    )
}

export default UpdateBeachPage
