import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { BeachesContext } from '../context/BeachesContext';
import BeachFinder from '../apis/BeachFinder';
import Reviews from '../components/ReviewsAndStars/Reviews';
import StarRating from '../components/ReviewsAndStars/StarRating';

const ReviewsAdmin = () => {
    const { id } = useParams();
    const { selectedBeach, setSelectedBeach } = useContext(
        BeachesContext
    );
    
    useEffect(() => {
        const fetchData = async() => {
            try {
                const response = await BeachFinder.get(`/${id}`);
                console.log(response);

                setSelectedBeach(response.data.data);
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    
    return (
        <div>
            {selectedBeach && (
                <>
                <h1 className="text-center display-1">
                {selectedBeach.beaches && selectedBeach.beaches.name}
                </h1>
                <div className="text-center">
                    <StarRating rating={selectedBeach.beaches && selectedBeach.beaches.average_rating} />
                    <span className="text-warning ml-1">
              {selectedBeach.beaches && selectedBeach.beaches.count
                ? `(${selectedBeach.beaches && selectedBeach.beaches.count})`
                : "(0)"}
            </span>
                </div>
                <div className="mt-3">
                    <Reviews reviews={selectedBeach.reviews} />
                </div>
                </>
            )}
        </div>
    );
};

export default ReviewsAdmin;
