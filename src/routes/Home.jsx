import React from 'react';
import Navbar from "../components/Navbar/Navbar";
import CarouselHP from '../components/CarouselHP';
import Footer from '../components/Footer';

const Home = () => {
    return (
        <div>
            <Navbar />
            <CarouselHP />
            <Footer />
        </div>
    )
}

export default Home
