import React from 'react';
import Navbar from "../components/Navbar/Navbar";
import Footer from '../components/Footer';
import BeachesListHome from '../components/Beaches/BeachesListHome';


const Beach = () => {
    return (
        <div>
            <Navbar />
            <BeachesListHome />
            <Footer />
        </div>
    )
}

export default Beach