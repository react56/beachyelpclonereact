import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import UpdateBeachPage from './routes/Admin/UpdateBeachPage';
import Home from './routes/Home';
import Beach from './routes/Beach';

import BeachDetailPage from './routes/BeachDetailPage';
import AdminHome from './routes/Admin/AdminHome';
import { BeachesContextProvider } from './context/BeachesContext';
import ReviewsAdmin from './routes/ReviewsAdmin';

const App = () => {
    return (
    <BeachesContextProvider>
        <div className="container">
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/beaches" component={Beach}/>
                    <Route exact path="/admin" component={AdminHome}/>
                    <Route exact path="/admin/beaches/:id/update" component={UpdateBeachPage}/>
                    <Route exact path="/admin/beaches/:id" component={ReviewsAdmin}/>
                    <Route exact path="/beaches/:id" component={BeachDetailPage}/>
                </Switch>
            </Router>
        </div>       
    </BeachesContextProvider>
    )
};

export default App;